
function myFunction1() {
  document.getElementById("w-30-1").style.display = "none";
}

function myFunction2() {
  document.getElementById("w-30-2").style.display = "none";
}

function myFunction3() {
  document.getElementById("w-30-3").style.display = "none";
}

function myFunction4() {
  document.getElementById("w-30-4").style.display = "block";
  let myTitle = document.getElementById("title").value;
  let myPrice = document.getElementById("price").value;

  document.getElementById("h2").innerText = myTitle;
  document.getElementById("h3").innerText = myPrice;
}

function myFunction5() {
  document.getElementById("w-30-4").style.display = "none";
}